#!/bin/bash -
#
#   Name    :   crash-texter.sh
#   Author  :   Richard Buchanan II
#   Brief   :   A script that sends an sms when apache or mysql crashes
#               Consult this list to find a carrier's email to sms method
#               http://mfitzp.io/list-of-email-to-sms-gateways/ 
#

set -o errexit      # exits if non-true exit status is returned
set -o nounset      # exits if unset vars are present
test -x /usr/sbin/apache2 || { echo "Apache is not installed" ; exit 1 ; }
test -x /usr/sbin/mysqld || { echo "MySQL is not installed" ; exit 1 ; }

# searches postfix's conf for the server's "mail." domain
mail_domain=$(grep -E -o "(mail[\.]).*([\.]?[com|org])$" /etc/postfix/main.cf)

# sets a no-shell account as the sender                                                   
mail_user="sysinform@${mail_domain}"
dest_email="number@placeholder"

err_log="/home/${main_user}/log/system-backup-err.log"


error_mailer () {
    script_name="$0"
    err_line="$1"
    echo "${full_date} - ${script_name}: failed on ${err_line}" >> "${err_log}"

    echo "there was a problem running crash-texter.sh on $HOSTNAME" | \
    mail -s "$HOSTNAME crash-texter.sh issue" -a "From: ${mail_user}" ${dest_email} ; 
    exit 1
}

trap 'error_mailer ${LINENO}' ERR


main () {
    for name in apache2 mysqld ; do
    # returns PID as a simple number
        if [[ "$(pidof "$name" | wc -w)" == 0 ]] ; then
    # texts if no PID is found and exits
            mail -s ""${name}" crashed on $HOSTNAME" -a "From: ${mail_user}" ${dest_email} ;
            exit 1 ;
        else 
            continue ;
        fi ;
    done
}


main 2>> "${err_log}"

exit 0
