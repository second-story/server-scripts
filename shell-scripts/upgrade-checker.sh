#!/bin/bash -
#
#   Name    :   upgrade-checker.sh
#   Author  :   Richard Buchanan II
#   Brief   :   a script to check for updates and send upgrade notifications
#               compatability for other package managers will be added
# 

set -o errexit      # exits if non-true exit status is returned
set -o nounset      # exits if unset vars are present

# searches the postfix main.cf file for the server's "mail." domain
mail_domain=$(grep -E -o "(mail[\.]).*([\.]?[com|org])$" /etc/postfix/main.cf)

# sets a no-shell account as the sender
mail_user="sysinform@${mail_domain}"
dest_email="email@placeholder"
full_date="$(date +%F)"

# check for updates and handle potential errors
apt update || { echo "The update check for $HOSTNAME failed on ${full_date}" | \
        mail -s "update failure for $HOSTNAME" -a "From: ${mail_user}" ${dest_email} ; 
        exit 1 ; 
        }    

# counts all updates while excluding apt's header
available_updates="$(apt list --upgradable | tail -n +2 | wc -l)"

if [[ ${available_updates} == 0 ]] ; then
    echo "There are no updates for $HOSTNAME on ${full_date}" | \
    mail -s "No bi-weekly updates for $HOSTNAME" -a "From: ${mail_user}" ${dest_email} ; 
else 
    echo "There are ${available_updates} updates for $HOSTNAME on ${full_date}" | \
    mail -s "New bi-weekly updates for $HOSTNAME" -a "From: ${mail_user}" ${dest_email} ;
fi

exit 0
