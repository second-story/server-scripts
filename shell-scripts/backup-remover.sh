#!/bin/bash -
#
#   Name    :   backup-remover.sh
#   Author  :   Richard Buchanan II
#   Brief   :   a script to remove old backups once they've been downloaded
#

set -o errexit      # exits if non-true exit status is returned
set -o nounset      # exits if unset vars are present
set -o errtrace     # inherits ERR on functions, substitutions, and sub-shells

# returns normal login instead of root
main_user="$(id -nu 1000)"

site_bak_path="/home/${main_user}/compressed-site-backups"
system_bak_path="/home/${main_user}/compressed-system-backups"
db_bak_path="/home/${main_user}/db-dumps"

full_date="$(date +%F)"

if [[ ! -d /home/"$(whoami)"/log ]] ; then
    mkdir /home/"$(whoami)"/log
fi

err_log="/home/${main_user}/log/backup-downloader-err.log"


error_logger () {
    script_name="$0"
    err_line="$1"
    echo "${full_date} - ${script_name}: failed on ${err_line}" >> "${err_log}"
    exit 1
}

trap 'error_logger ${LINENO}' ERR


main () {
    for i in "${site_bak_path}" "${system_bak_path}" "${db_bak_path}" ; do
        if [[ ! -z "$(ls "${i}")" ]] ; then
            find "${i}" -maxdepth 1 -type f -exec echo "deleting {}" \; -delete ;
        else
            echo "Nothing found in ${i}"
            continue
        fi
    done

    echo "All old backups have been removed."
}


# tee reads STDERR as its input, appends specific error messages to err_log, 
# then redirects its output back to STDERR
main 2>> >(tee -a "${err_log}" >&2)

exit 0
