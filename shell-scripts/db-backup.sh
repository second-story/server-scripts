#!/bin/bash -
#
#   Name    :   db-backup.sh
#   Author  :   Richard Buchanan II
#   Brief   :   a database backup script
#               add to a non-root crontab and create /home/user/.my.cnf to run properly
#

set -o errexit      # exits if non-true exit status is returned
set -o nounset      # exits if unset vars are present
test -x /usr/bin/mysqldump || { echo "mysqldump is not installed" ; exit 1 ; }

# returns normal login instead of root
main_user="$(id -nu 1000)" 

# searches postfix's conf for the server's "mail." domain
mail_domain=$(grep -E -o "(mail[\.]).*([\.]?[com|org])$" /etc/postfix/main.cf)             

# sets a no-shell account as the sender                                                   
mail_user="sysinform@${mail_domain}"
dest_email="email@placeholder"

full_date="$(date +%F)"
backup_path="/home/${main_user}/db-backups"
db_output="${HOSTNAME}-db_${full_date}.sql"
space_used="$(df -h | grep home | awk '{ print substr($5,0,3) }')"

if [[ ! -d /home/"${main_user}"/log ]] ; then
    mkdir /home/"${main_user}"/log
fi

err_log="/home/${main_user}/log/db-backup-err.log"

error_mailer () {
    script_name="$0"
    err_line="$1"
    echo "${full_date} - ${script_name}: failed on ${err_line}" >> "${err_log}"
    
    echo "the $HOSTNAME db dump failed on ${full_date}" | \
    mail -s "$HOSTNAME db dump failure" -a "From: ${mail_user}" ${dest_email}
    exit 1
}

trap 'error_mailer ${LINENO}' ERR

main () {
    find "${backup_path}" -type f -mtime +30 -delete

    # create the db dump and email its completion status
    /usr/bin/mysqldump -u user-name db-name > "${backup_path}"/"${db_output}"

    # space usage for email success notification
    backup_space="$(du -chs "${backup_path}" 2>/dev/null | awk '{print $1}' | uniq)"
    backup_count="$(ls "${backup_path}" | wc -l)"

    # increase the db dump's security
    chmod 600 "${backup_path}"/"${db_output}"

    # the db dump worked
    echo "the $HOSTNAME db dump was successful on ${full_date}.

    There are "${backup_count}" backups @ "${backup_space}" that need attention.
        
    ~/ currently has ${space_used} space used." | \
    mail -s "$HOSTNAME db dump complete" -a "From: ${mail_user}" ${dest_email}
}


main 2>> "${err_log}"

exit 0
