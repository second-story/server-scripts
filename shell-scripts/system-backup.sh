#!/bin/bash -
#
#   Name    :   system-backup.sh
#   Author  :   Richard Buchanan II
#   Brief   :   a script to archive /etc, /var/log, and currently installed packages
#

set -o errexit      # exits if non-true exit status is returned
set -o nounset      # exits if unset vars are present

# returns normal login instead of root
main_user="$(id -nu 1000)"

# searches postfix's conf for the server's "mail." domain
mail_domain=$(grep -E -o "(mail[\.]).*([\.]?[com|org])$" /etc/postfix/main.cf)             

# sets a no-shell account as the sender                                       
mail_user="sysinform@${mail_domain}"
dest_email="email@placeholder"

full_date="$(date +%F)"
backup_path="/home/${main_user}/compressed-system-backups"
backup_output="${HOSTNAME}-system-backup_${full_date}.tar.gz"
current_packages="/home/${main_user}/$HOSTNAME-packages_${full_date}"

space_used="$(df -h | grep home | awk '{ print substr($5,0,3) }')"

if [[ ! -d /home/"${main_user}"/log ]] ; then
    mkdir /home/"${main_user}"/log
fi

err_log="/home/${main_user}/log/system-backup-err.log"

error_mailer () {
    script_name="$0"
    err_line="$1"
    rm "${current_packages}"
    echo "${full_date} - ${script_name}: failed on ${err_line}" >> "${err_log}"

    echo "the $HOSTNAME system backup failed on ${full_date}" | \
    mail -s "$HOSTNAME system backup failure" -a "From: ${mail_user}" ${dest_email} ; 
    exit 1
}

trap 'error_mailer ${LINENO}' ERR

main () {
    find "${backup_path}" -type f -mtime +30 -delete

    dpkg --get-selections > "${current_packages}"

    tar -cvzf "${backup_path}"/"${backup_output}" /etc /var/log "${current_packages}"

    # current package list isn't needed anymore
    rm "${current_packages}"

    # space usage for email success notification
    backup_space="$(du -chs "${backup_path}" 2>/dev/null | awk '{print $1}' | uniq)"
    backup_count="$(ls "${backup_path}" | wc -l)"

    # increase backup security and mail a success notification
    chmod 600 "${backup_path}"/"${backup_output}"
    chown "${main_user}":"${main_user}" "${backup_path}"/"${backup_output}"

    echo "the $HOSTNAME system backup was successful on ${full_date}.
        
    There are ${backup_count} backups @ ${backup_space} that need attention.
        
    ~/ currently has ${space_used} space used." | \
    mail -s "$HOSTNAME system backup complete" -a "From: ${mail_user}" ${dest_email}
}


main 2>> "${err_log}"

exit 0
