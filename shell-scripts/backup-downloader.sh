#!/bin/bash -
#
#   Name    :   backup-downloader.sh
#   Author  :   Richard Buchanan II
#   Brief   :   a script to pull all backups from a remote server
#

set -o errexit      # exits if non-true exit status is returned
set -o nounset      # exits if unset vars are present
set -o errtrace     # inherits ERR on functions, substitutions, and sub-shells

bak_base="/home/$(whoami)/backups"

# the slash is used for directory traversal below
this_month="$(date +%Y/%m)"
full_date="$(date +%F)"

if [[ ! -d /home/"$(whoami)"/log ]] ; then
    mkdir /home/"$(whoami)"/log
fi

err_log="/home/$(whoami)/log/backup-downloader-err.log"

error_logger () {
    script_name="$0"
    err_line="$1"
    echo "${full_date} - ${script_name}: failed on ${err_line}" >> "${err_log}"
    exit 1
}

trap 'error_logger ${LINENO}' ERR

main () {
    rem_host="$1"
    rem_user="$(ssh "${rem_host}" 'id -nu 1000')"

    rem_site_bak_path="/home/${rem_user}/compressed-site-backups"
    rem_system_bak_path="/home/${rem_user}/compressed-system-backups"
    rem_db_bak_path="/home/${rem_user}/db-dumps"

    if [[ ! -d "${bak_base}"/"${rem_host}"/"${this_month}" ]] ; then
        mkdir -p "${bak_base}"/"${rem_host}"/"${this_month}"
    fi

    cd "${bak_base}"/"${rem_host}"/"${this_month}"
    rsync -avhz --progress --verbose "${rem_host}":"${rem_site_bak_path}" .
    rsync -avhz --progress --verbose "${rem_host}":"${rem_system_bak_path}" .
    rsync -avhz --progress --verbose "${rem_host}":"${rem_db_bak_path}" .

    # increase the backup's security
    find "${bak_base}"/"${rem_host}"/"${this_month}" -type d -exec chmod 700 {} \;
    find "${bak_base}"/"${rem_host}"/"${this_month}" -type f -exec chmod 600 {} \;

    echo "All backups on ${rem_host} have been successfully downloaded."
}


# tee reads STDERR as its input, appends specific error messages to err_log, 
# then redirects its output back to STDERR
main "$1" 2>> >(tee -a "${err_log}" >&2)

exit 0
