#!/usr/bin/env python3

"""
When paired with bad-ip-collector.py, this program finds repeat offenders in
a tracking file, and adds them and all IPs marked for immediate blocking to 
a new, safe file that has whitelisted IPs removed
"""

import notification_mailer as mail_lib
import file_len as len_lib
import collections
import os
import re
import socket
import subprocess
import sys

__author__ = 'Richard Buchanan II'
__copyright__ = 'August 2018'
__credits__ = ['Richard Buchanan II']
__license__ = 'GPL v3'
__version__ = '1.0'
__maintainer__ = 'Richard Buchanan II'
__status__ = 'Production'


# returns normal login instead of root
MAIN_USER = subprocess.getoutput('id -nu 1000')
HOSTNAME = socket.gethostname()

IP_DIR = '/home/{}/ip-trackers'.format(MAIN_USER)
IP_WHITELIST = ['127.0.0.1']

BLOCK_FILE = '{}/ip-block-file'.format(IP_DIR)
TRACK_FILE = '{}/ip-track-file'.format(IP_DIR)
# safe files have the IPs in the whitelist removed
SAFE_BLOCK_FILE = '{}/safe-ip-block-file'.format(IP_DIR)
VERIF_FILE = '{}/ip-verif-file'.format(IP_DIR)

IP_PATTERN = re.compile(r'(([0-9]{1,3}[\.]){3}[0-9]{1,3})')
COUNT_PATTERN = re.compile(r'([0-9]+) offense\(s\)')


def track_file_parser():
    '''
    Searches for duplicates in the IP track file, and adds them to a list for
    use in safe_file_writer()
    '''
    bad_ip_list = []
    formatted_list = []

    with open(TRACK_FILE, 'r') as f:
        for line in f:
            bad_ips = IP_PATTERN.findall(line)
            if bad_ips:
                bad_ip_list.append(bad_ips[0][0])

    for ip, counter in collections.Counter(bad_ip_list).items():
        formatted_list.append('{0} offense(s) from {1}'.format(counter, ip))

    return formatted_list


def safe_file_writer():
    '''
    Adds all track file duplicates and block file entries to the safe 
    block file, and removes whitelisted IPs from the safe file
    '''
    formatted_list = track_file_parser()

    with open(SAFE_BLOCK_FILE, 'a') as f:
        for i in formatted_list:
            # this is where the X offense(s)... comes into play
            if i[0][0] == 1:
                pass
            else:
                # this use of re is for quickly bypassing all date strings
                bad_ips = IP_PATTERN.findall(i)
                if bad_ips:
                    f.write(bad_ips[0][0])
                    f.write('\n')

    with open(BLOCK_FILE, 'r') as orig_file, open(SAFE_BLOCK_FILE, 'a') as safe_file:
        for line in orig_file:
            if line not in IP_WHITELIST:
                    bad_ips = IP_PATTERN.findall(line)
                    if bad_ips:
                        safe_file.write(bad_ips[0][0])
                        safe_file.write('\n')


def main():
    '''
    Counts the lines in the safe block file, sorts the file, and then sends an
    email notification with the total count
    '''
    safe_file_len = len_lib.file_len(SAFE_BLOCK_FILE)

    # sort is used because there's no reason to again open / close the files
    # for reading and writing
    subprocess.run('sort -n {}'.format(SAFE_BLOCK_FILE), shell=True)

    notif_subj = 'New IPs to block on {}'.format(HOSTNAME)
    notif_text = 'Collected {} IPs to ban from the block / track files'.format(safe_file_len)

    mail_lib.notification_mailer(notif_subj, notif_text, 'send_email')


if __name__ == '__main__':
    safe_file_writer()
    main()
    os.remove(BLOCK_FILE)
    os.remove(TRACK_FILE)
    os.remove(VERIF_FILE)
    sys.exit(0)
