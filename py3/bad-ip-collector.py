#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Combs through auth logs and Apache error logs for malicious IPs.  Once found, offending
IPs are added to one of two files each day.

The first file collects repeat offenders for immediate banning.  The second tracks IPs
that cause problems once per day, only to cause problems again later on.

Meant to be run at 23:59, to capture all errors for that day
'''

import notification_mailer as mail_lib
import file_len as len_lib
import collections
import datetime
import os
import re
import socket
import shutil
import subprocess
import sys
import textwrap

__author__ = 'Richard Buchanan II'
__copyright__ = 'August 2018'
__credits__ = ['Richard Buchanan II']
__license__ = 'GPL v3'
__version__ = '1.0'
__maintainer__ = 'Richard Buchanan II'
__status__ = 'Production'


MAIN_USER = subprocess.getoutput('id -nu 1000')

if not os.path.exists('home/{}/ip-trackers'.format(MAIN_USER)):
    os.makedirs('/home/{}/ip-trackers'.format(MAIN_USER), exist_ok=True)

IP_DIR = '/home/{}/ip-trackers'.format(MAIN_USER)

OS_LIST = ['Debian', 'Ubuntu', 'CentOS']
HOST_OS = subprocess.getoutput('head -n 1 /etc/os-release')
HOSTNAME = socket.gethostname()

# the leading 0 is removed from the day to conform to Debian / Ubuntu's auth.log
PART_DATE = datetime.datetime.now().strftime('%b  %d').replace('0', '')
DATE_STRING = 'IPs added on {}'.format(PART_DATE)

if OS_LIST[0] or OS_LIST[1] in HOST_OS:
    shutil.copy('/var/log/auth.log', '{}/auth.log.tmp'.format(IP_DIR))
    shutil.copy('/var/log/apache2/error.log', '{}/apache.err.log.tmp'.format(IP_DIR))
    TMP_AUTH_LOG = '{}/auth.log.tmp'.format(IP_DIR)
    TMP_WWW_ERR_LOG = '{}/apache.err.log.tmp'.format(IP_DIR)
elif OS_LIST[2] in HOST_OS:
    shutil.copy('/var/log/secure', '{}/secure.tmp'.format(IP_DIR))
    shutil.copy('/var/log/httpd/error.log', '{}/httpd.err.log.tmp'.format(IP_DIR))
    TMP_AUTH_LOG = '{}/secure.tmp'.format(IP_DIR)
    TMP_WWW_ERR_LOG = '{}/httpd.err.log.tmp'.format(IP_DIR)
else:
    print('Unrecognized OS.  The supported distros are Debian, Ubuntu, and Centos')
    sys.exit(1)


BLOCK_FILE_PATH = '{}/ip-block-file'.format(IP_DIR)
TRACK_FILE_PATH = '{}/ip-track-file'.format(IP_DIR)
VERIF_FILE_PATH = '{}/ip-verif-file'.format(IP_DIR)

AUTH_ERR_PATTERN = re.compile(r'(^.*([Ii]nvalid user|not allowed).*$)')
WWW_ERR_PATTERN = re.compile(r'(^.*(evasive|not found|forbidden).*$)')

IP_PATTERN = re.compile(r'(([0-9]{1,3}[\.]){3}[0-9]{1,3})')
# the below pattern is used to filter for same-day errors
DAY_PATTERN = re.compile(r'^{}.*'.format(PART_DATE))
COUNT_PATTERN = re.compile(r'([0-9]+) offense\(s\)')

ORIG_BLOCK_LEN = len_lib.file_len(BLOCK_FILE_PATH)
ORIG_TRACK_LEN = len_lib.file_len(TRACK_FILE_PATH)


def err_parser(tmp_file, regex_pattern):
    '''
    Reads error messages based on the supplied regex pattern, collects the offending
    IPs, and then counts an IP's number of offenses for use in file_writer()
    '''
    bad_ip_list = []
    formatted_list = []

    with open(tmp_file, 'r') as f:
        for line in f:
            auth_err = regex_pattern.search(line)
            # empty matches are treated as False, so the below test will skip them
            if auth_err:
                for err in auth_err.groups():
                    day_err = DAY_PATTERN.findall(err)
                    for filter_err in day_err:
                        bad_ips = IP_PATTERN.findall(filter_err)
                        bad_ip_list.append(bad_ips[0][0])

    # the message is verbose to simplify counting IPs in file_writer()
    for ip, counter in collections.Counter(bad_ip_list).items():
        formatted_list.append('{0} offense(s) from {1}'.format(counter, ip))

    return formatted_list


def file_writer(tmp_file, regex_pattern):
    '''
    Depending on how many offenses an IP has, it is added either to a file for
    immediate banning, or a file for tracking across several days.  A verification file
    is also written for auditing
    '''
    formatted_list = err_parser(tmp_file, regex_pattern)

    with open(VERIF_FILE_PATH, 'a') as f:
        # stops DATE_STRING from being written for empty lists
        if not formatted_list:
            pass
        else:
            f.write(DATE_STRING)
            f.write('\n')
            for line in formatted_list:
                f.write(line)
                f.write('\n')

    with open(BLOCK_FILE_PATH, 'a') as block_file, open(TRACK_FILE_PATH, 'a') as track_file:
        if not formatted_list:
            pass
        else:
            track_file.write(DATE_STRING)
            track_file.write('\n')
            block_file.write(DATE_STRING)
            block_file.write('\n')
            for line in formatted_list:
                block_count = COUNT_PATTERN.findall(line)
                bad_ips = IP_PATTERN.findall(line)
                if int(block_count[0]) == 1:
                    track_file.write(bad_ips[0][0])
                    track_file.write('\n')
                else:
                    block_file.write(bad_ips[0][0])
                    block_file.write('\n')


def main():
    '''
    Emails a notification of the day's IP count
    '''
    new_block_len = len_lib.file_len(BLOCK_FILE_PATH)
    new_track_len = len_lib.file_len(TRACK_FILE_PATH)
    block_file_diff = int(new_block_len) - int(ORIG_BLOCK_LEN)
    track_file_diff = int(new_track_len) - int(ORIG_TRACK_LEN)

    notif_subj = '{} IP block and track files were updated'.format(HOSTNAME)
    notif_text = textwrap.dedent('''
    {0} IPs were added to the block file, and {1} were added to the tracker.
    The block file now has {2} entries, and the track file now has {3} entries.
    '''.format(block_file_diff, track_file_diff, new_block_len, new_track_len))

    mail_lib.notification_mailer(notif_subj, notif_text, 'send_email')


if __name__ == '__main__':
    file_writer(TMP_AUTH_LOG, AUTH_ERR_PATTERN)
    file_writer(TMP_WWW_ERR_LOG, WWW_ERR_PATTERN)
    main()
    os.remove(TMP_AUTH_LOG)
    os.remove(TMP_WWW_ERR_LOG)
    sys.exit(0)
