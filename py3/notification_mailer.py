#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
A script that sends an email or text notification when required by another script

Consult this list to find your carrier's email to sms gateway:
https://email2sms.info/.
'''

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import logging
import smtplib
import subprocess
import sys

__author__ = 'Richard Buchanan II'
__copyright__ = 'July 2018'
__credits__ = ['Richard Buchanan II']
__license__ = 'GPL v3'
__version__ = '1.1'
__maintainer__ = 'Richard Buchanan II'
__status__ = 'Production'


MAIN_USER = subprocess.getoutput('whoami')
LOG_PATH = '/home/{}/log'.format(MAIN_USER)

# searches the postfix main.cf file for the server's "mail." domain
MAIL_DOMAIN = subprocess.getoutput("grep -Eo '(mail[\.]).*([\.]?[com|org])$' /etc/postfix/main.cf")
MAIL_SRC = 'src-placeholder@{}'.format(MAIL_DOMAIN)

logging.basicConfig(level=logging.ERROR,
    format='%(asctime)s - %(module)10s @ %(funcName)s, line %(lineno)d - %(levelname)10s - %(message)s',
    datefmt='%Y-%m-%d @ %H:%M:%S',
    filename='{}/notification_mailer.log'.format(LOG_PATH),
    filemode='a'
)

logger = logging.getLogger(__name__)


def notification_mailer(subject, message, text_or_email):
    '''
    Handles sending email with the subject and text provided by another script
    '''

    # switches to texting if an immediate notification is required
    if text_or_email == 'send_text':
        MAIL_DEST = ['phone-number@smsgateway.com']
    elif text_or_email == 'send_email':
        MAIL_DEST = 'dest@placeholder'
    else:
        logger.error('Wrong option {} passed to text_or_email'.format(text_or_email))
        sys.exit(1)

    mail_mssg = MIMEMultipart('plain')
    mail_mssg['From'] = MAIL_SRC
    mail_mssg['To'] = MAIL_DEST
    mail_mssg['Subject'] = subject
    mail_text = message

    mail_mssg.attach(MIMEText(mail_text))
    mail_server = smtplib.SMTP('localhost', port)

    try:
        mail_server.starttls()
        mail_server.ehlo(name='{}'.format(MAIL_DOMAIN))
        mail_server.sendmail(MAIL_SRC, MAIL_DEST, mail_mssg.as_string())
    except Exception as mail_err:
        logger.exception(mail_err)
    finally:
        mail_server.quit()


if __name__ == "__main__":
    main()
    sys.exit(0)
