#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
A script that creates individual database dumps for systems that host multiple dbs
'''

import notification_mailer as mail_lib
import datetime
import logging
import os
import re
import sys
import socket
import subprocess
import textwrap

__author__ = 'Richard Buchanan II'
__copyright__ = 'July 2018'
__credits__ = ['Richard Buchanan II']
__license__ = 'GPL v3'
__version__ = '1.1'
__maintainer__ = 'Richard Buchanan II'
__status__ = 'Production'


MAIN_USER = subprocess.getoutput('whoami')
HOSTNAME = socket.gethostname()

# appends to the end of the db dump
FULL_DATE = datetime.datetime.now().strftime('%Y-%m-%d')

HOME_PATH = '/home/{}'.format(MAIN_USER)
LOG_PATH = '{}/log'.format(HOME_PATH)
BAK_PATH = '{}/db-dumps'.format(HOME_PATH)
CNF_PATH = '{}/.mysql'.format(HOME_PATH)

DB_LIST = ['db1.cnf'.format(CNF_PATH), 'db2.cnf'.format(CNF_PATH)]

logging.basicConfig(level=logging.ERROR,
    format='%(asctime)s - %(module)10s @ %(funcName)s, line %(lineno)d - %(levelname)10s - %(message)s',
    datefmt='%Y-%m-%d @ %H:%M:%S',
    filename='{}/db-backup_py.log'.format(LOG_PATH),
    filemode='a'
    )

logger = logging.getLogger(__name__)


def conf_parser(conf_file):
    '''
    Reads through supplied conf files and pulls the needed info for use in mysqldump
    '''

    user_pattern = re.compile(r'^(user=)(.*)$')
    db_pattern = re.compile(r'^(database=)(.*)$')
    host_pattern = re.compile(r'^(host=)(.*)$')

    db_dict = {'user': '', 'db': '', 'host': ''}

    with open (conf_file, 'r') as f:
        for line in f:
            db_user = user_pattern.search(line)
            db_name = db_pattern.search(line)
            db_host = host_pattern.search(line)

            if db_user:
                db_dict['user'] = db_user.group(2)
            if db_name:
                db_dict['db'] = db_name.group(2)
            if db_host:
                db_dict['host'] = db_host.group(2)

    return db_dict


def backup_creator(conf_file):
    '''
    Takes all info from conf_parser and feeds it into mysqldump
    '''

    db_dict = conf_parser(conf_file)

    # removes the quotes from the dict for proper formatting
    db_user = re.sub("'", '', db_dict['user'])
    db_name = re.sub("'", '', db_dict['db'])
    db_host = re.sub("'", '', db_dict['host'])

    bak_file = '{0}/{1}-{2}-db_{3}.sql'.format(BAK_PATH, HOSTNAME, db_name, FULL_DATE)

    with open(bak_file, 'wb') as db_dump:
        try:
            mysqldump_call = subprocess.Popen([
                '/usr/bin/mysqldump', '--defaults-group-suffix={}'.format(db_name),
                '--defaults-file={}/{}.cnf'.format(CNF_PATH, db_name), 
                '-u', '{}'.format(db_user), '{}'.format(db_name)
                ],
                stdout=db_dump
            )

            mysqldump_call.communicate()[0]
        except Exception as e:
            logger.exception(e)

            notif_subj = 'db-backup.py failed on {}'.format(HOSTNAME)
            notif_text = textwrap.dedent('''
            db-backup.py failed while running mysqldump.
            The exception is as follows:

            {}'''.format(e))

            mail_lib.notification_mailer(notif_subj, notif_text, 'send_email')
            sys.exit(1)

    # lock down the db dump's permissions
    os.chmod(bak_file, 0o600)


def main():
    '''
    determines how many backups exist and hwo much space they occupy, and then sends a 
    success notification email that includes that info
    '''

    # backup space usage for email notifications
    du_call = subprocess.Popen(['du', '-chs', BAK_PATH],
        stdout=subprocess.PIPE,
        stderr=subprocess.DEVNULL
    )

    awk_call = subprocess.Popen(['awk', '{print $1}'],
        stdin=du_call.stdout,
        stdout=subprocess.PIPE
    )

    uniq_call = subprocess.Popen('uniq',
        stdin=awk_call.stdout,
        stdout=subprocess.PIPE
        )

    bak_size = uniq_call.communicate()[0].decode('utf-8').rstrip()

    # counts the total available backups
    ls_call = subprocess.Popen(['ls', BAK_PATH],
        stdout=subprocess.PIPE
        )
        
    wc_call = subprocess.Popen(['wc', '-l'],
        stdin=ls_call.stdout,
        stdout=subprocess.PIPE
        )
        
    bak_count = wc_call.communicate()[0].decode('utf-8').rstrip()

    # total space usage on the home partition
    home_space_used = subprocess.getoutput("df -h | grep home | awk '{ print substr($5,0,3) }'")


    # and now to handle the notification email
    notif_subj = 'Completed db backups on {}'.format(HOSTNAME)
    notif_text = textwrap.dedent('''
    Successfully backed up all dbs on {0}.
    
    There are now {1} backups @ {2} that need attention

    ~/ currently has {3} space used.
    '''.format(HOSTNAME, bak_count, bak_size, home_space_used))

    mail_lib.notification_mailer(notif_subj, notif_text, 'send_email')


if __name__ == "__main__":
    backup_creator(DB_LIST[0])
    backup_creator(DB_LIST[1])
    main()
    sys.exit(0)
