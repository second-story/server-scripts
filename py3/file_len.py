#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
A script to help other programs count all lines in a file.
'''

import sys

__author__ = 'Richard Buchanan II'
__copyright__ = 'August 2018'
__credits__ = ['Richard Buchanan II']
__license__ = 'GPL v3'
__version__ = '1.0'
__maintainer__ = 'Richard Buchanan II'
__status__ = 'Production'


def file_len(file_name):
    '''
    Counts the lines in a given file
    '''
    with open(file_name, 'r') as f:
        count = 0
        for line in f:
            count += 1
    return count


if __name__ == "__main__":
    main()
    sys.exit(0)
