#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
A script to call uptime and check CPU load values as part of a cron job, and text
an alert message if the CPU is overloaded for any consistent length of time
between 10 minute intervals.
'''

import notification_mailer as mail_lib
import datetime
import socket
import subprocess
import sys
import textwrap

__author__ = 'Richard Buchanan II'
__copyright__ = 'March 2018'
__credits__ = ['Richard Buchanan II']
__license__ = 'GPL v3'
__version__ = '1.0'
__maintainer__ = 'Richard Buchanan II'
__status__ = 'Production'


MAIN_USER = subprocess.getoutput('whoami')
HOSTNAME = socket.gethostname()

# get the total CPU cores
CPU_CORES = subprocess.getoutput("lscpu | grep 'CPU(s):' | grep -m 1 -Eo [0-9]")

# define half load, since uptime load output is based on total CPU cores
# (i.e. 1.00 is 100% load for a 1-core CPU, but 25% load for a 4-core CPU)
HALF_LOAD = float(CPU_CORES) * 0.5

# searches the postfix main.cf file for the server's "mail." domain
# re isn't used because subprocess was already imported for lscpu, uptime, etc
MAIL_DOMAIN = subprocess.getoutput("grep -Eo '(mail[\.]).*([\.]?[com|org])$' /etc/postfix/main.cf")

load_list = []


def uptime_list_append():
    '''
    Formats uptime single-minute values and call times as floats.  If CPU load
    is 50% or above, then a tuple of CPU load and uptime call times is appended
    to load_list.  If CPU load is under 50%, then this program immediately exits.
    '''

    # grab a single-minute CPU load value from uptime
    # uptime_minute_load is normally a string thanks to the trailing comma
    uptime_call = subprocess.getoutput('uptime')
    uptime_split = uptime_call.split(' ')
    uptime_minute_load = float(uptime_split[13].strip(','))

    # a dot is used between hours and minutes for arithmetic in
    # load_list_time_checker() > time_check
    uptime_run_time = datetime.datetime.now().strftime('%H.%M')
    uptime_run_day = datetime.datetime.now().strftime('%w')

    if uptime_minute_load >= HALF_LOAD:
        load_list.append((uptime_minute_load, uptime_run_time, uptime_run_day))
    else:
        sys.exit(0)


def load_list_time_checker():
    '''
    Clears load_list entries that are older than 10 minutes, or exits if
    load_list only contains a single entry.
    '''

    # exits if load_list only has 1 entry to prevent list index errors
    if len(load_list) == 1:
        sys.exit(0)

    # numbers bewteen 0.01 and 0.09, or -23.51 and -23.59 are less than 10 minutes
    time_check = load_list[-1][1] - load_list[-2][1]

    # if day_check evaluates to False, then the previous load_list entry is
    # older than 1 day
    day_check = load_list[-1][2] == load_list[-2][2]

    # hacky, but the alternative is to create a range function for floats, or
    # check for a number that's greater than 0.1 and greater than -23.5, but
    # doesn't exceed 0.01, which will wrongly short-circuit line 90 during
    # any time shortly after midnight
    pos_vals = [0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09]
    neg_vals = [-23.51, -23.52, -23.53, -23.54, -23.55, -23.56, -23.57, -23.58, -23.59]

    # clear previous list entries if the most recent is older than 10 minutes
    # usable times between 23:50 and 00:10 will return negatives less than -23.5
    if time_check in pos_vals or time_check in neg_vals or day_check is True:
        pass
    else:
        del load_list[:-1]


def main():
    '''
    Defines the average CPU load, alert message, sender, and recipient, and
    sends an alert text if load_list has 3 entries within a 10 minute time span
    '''
    if len(load_list) == 3:
        load_avg = (load_list[0][0] + load_list[1][0] + load_list[2][0]) * 3


        notif_subj = 'High CPU load on {2}'.format(HOSTNAME)
        notif_text = textwrap.dedent('''
        {0} has had high CPU load for 5+ minutes, with an average load of {1}.
        Please troubleshoot ASAP.
        '''.format(HOSTNAME, load_avg))

        mail_lib.notification_mailer(notif_subj, notif_text, 'send_text')


if __name__ == "__main__":
    uptime_list_append()
    load_list_time_checker()
    main()
    sys.exit(0)
