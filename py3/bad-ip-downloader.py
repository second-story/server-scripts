#!/usr/bin/env python3

"""
When paired with bad-ip-{collector,organizer}.py, this program creates a cumulative
block file from host-specific block files that has all duplicate offenders removed.
This file can then be fed into ipset, firewall-cmd, etc
"""

import os
import subprocess
import sys

__author__ = 'Richard Buchanan II'
__copyright__ = 'August 2018'
__credits__ = ['Richard Buchanan II']
__license__ = 'GPL v3'
__version__ = '1.0'
__maintainer__ = 'Richard Buchanan II'
__status__ = 'Production'


MAIN_USER = subprocess.getoutput('whoami')
HOME_PATH = '/home/{}'.format(MAIN_USER)
HOST_DICT = {
            'host1': 'username',
            'host2': 'username',
            'host3': 'username'
            }


def blocklist_downloader():
    '''
    downloads all IP block files from the hosts specified in HOST_DICT
    '''
    scp_list = []
    for host, remote_user in HOST_DICT.items():
        scp_call = subprocess.Popen('scp {}:/home/{}/ip-trackers/safe-ip-block-file \
            {}/{}-block-list'.format(host, remote_user, HOME_PATH, remote_user), shell=True,
            stdout=subprocess.PIPE
            )
        # the output isn't optional because it's the login prompt.  Without this, SSH 
        # will terminate the connection immediately and log it as a failed attempt 
        scp_output = scp_call.communicate()[0].decode('utf-8')
        print(scp_output)
        scp_list.append('{}/{}-block-list'.format(HOME_PATH, remote_user))
    return scp_list


def main():
    '''
    Combines the block files from blocklist_downloader(), sorts the results and
    removes duplicates, and removes the host-specific block files when finished
    '''
    scp_list = blocklist_downloader()
    total_ips = '{}/total-ips'.format(HOME_PATH)

    for filename in scp_list:
        with open(filename, 'r') as ip_file, open(total_ips, 'a') as total:
            for line in ip_file:
                total.write(line)
            print('finished writing {} to the cumulative block list'.format(filename))
            os.remove(filename)

    subprocess.run('sort -n {} | uniq > sorted_ips'.format(total_ips), shell=True)
    os.remove(total_ips)
    print('the cumulative block list is ready for use')


if __name__ == '__main__':
    main()
    sys.exit(0)
