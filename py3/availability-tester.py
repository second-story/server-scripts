#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
A script to check host availability and send text notifications if a host is offline.
Meant to be run on a dedicated monitoring server.

Consult this list to find your carrier's email to sms gateway:
https://email2sms.info/.
'''

import notification_mailer as mail_lib
import logging
import re
import socket
import subprocess
import sys
import textwrap

__author__ = 'Richard Buchanan II'
__copyright__ = 'March 2018'
__credits__ = ['Richard Buchanan II']
__license__ = 'GPL v3'
__version__ = '1.1'
__maintainer__ = 'Richard Buchanan II'
__status__ = 'Production'


# returns normal login instead of root
MAIN_USER = subprocess.getoutput('id -nu 1000')
LOG_PATH = '/home/{}/log'.format(MAIN_USER)
HOSTNAME = socket.gethostname()

# searches the postfix main.cf file for the server's "mail." domain
MAIL_DOMAIN = subprocess.getoutput("grep -Eo '(mail[\.]).*([\.]?[com|org])$' /etc/postfix/main.cf")
MAIL_SRC = 'src-placeholder@{}'.format(MAIL_DOMAIN)
MAIL_DEST = ['dest@placeholder']

DOMAIN_LIST = ['placeholder.dom1', 'placeholder.dom2', 'placeholder.dom3']

logging.basicConfig(level=logging.ERROR,
    format='%(asctime)s - %(module)10s @ %(funcName)s, line %(lineno)d - %(levelname)10s - %(message)s',
    datefmt='%Y-%m-%d @ %H:%M:%S',
    filename='{}/availability_tester.log'.format(LOG_PATH),
    filemode='a'
    )

logger = logging.getLogger(__name__)


def domain_tester():
    '''
    sends 5 pings to the given domain, searches for the packet loss percentage, and then
    scrubs the % symbol from the result
    '''

    availability_dict = {}

    try:
        for domain in DOMAIN_LIST:
            ping_call = subprocess.Popen(['ping', '-c5', domain],
                stdout=subprocess.PIPE
            )

            ping_result = ping_call.communicate()[0].decode('utf-8')
            ping_pattern = re.compile('[0-9]+%')
            ping_fail_percent = ping_pattern.search(ping_result).group()
            percent_char_strip = re.sub(r'%', '', ping_fail_percent)

            availability_dict.update({domain: int(percent_char_strip)})

        return availability_dict
    except Exception as e:
        logger.exception(e)

        notif_subj = 'availability-tester failed on {}'.format(HOSTNAME)
        notif_text = textwrap.dedent('''
        availability-tester failed during execution.
        The exception is as follows:
        
        {}'''.format(e))

        mail_lib.notification_mailer(notif_subj, notif_text, 'send_text')
        sys.exit(1)


def main():
    '''
    uses the domain_tester() result to text a notification if the domain is unreachable
    '''

    availability_dict = domain_tester()

    for domain, fail_percent in availability_dict.items():
        if fail_percent == 0:
            pass
        else:
            notif_subj = '{} may be down'.format(domain)
            notif_text = '''{0} is {1} unreachable.  Please troubleshoot ASAP.
            '''.format(domain, fail_percent)

            mail_lib.notification_mailer(notif_subj, notif_text, 'send_text')


if __name__ == "__main__":
    main()
    sys.exit(0)
