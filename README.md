A collection of scripts for automating tasks on a Linux server (web, file, etc).

The scripts and their purpose are as follows:


## Automation
* notification_mailer.py
   - Handles email or text notifications for scripts that require them

* upgrade-checker.sh
   - Checks for updates and sends email notifications on a bi-weekly basis

## Backups
* backup-downloader.sh
   - Downloads all backups from a remote host to a home system

* backup-remover.sh
   - Removes all old backups once they've been downloaded

* db-backup.py
   - Creates individual database dumps for systems hosting any number of MySQL databases

* db-backup.sh
   - A script for backing up MySQL databases.  Can only handle a single database

* site-backup.sh
   - A webserver backup script.  Saves any directory in /var/www/html

* system-backup.sh
   - A system backup script.  Saves various /etc locations, the contents of /var/log, and  currently installed packages


## Monitoring
* availability-tester.py
   - Checks remote host availability and sends a text within 5 minutes of detected outages.  Meant to run on a monitoring server.

* crash-texter.sh
   - Monitors Apache and MySQL processes and sends a text within 5 minutes of a crash

* load-watcher.py
   - Monitors CPU load and sends alert texts if the server is overloaded


All scripts are licensed under the Lesser GNU Public License v3.0, which is included in 
COPYING.LESSER.
